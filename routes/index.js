var express = require('express');
var router = express.Router();
var fs = require('fs');

router.all('/*', function(req, res, next){
    console.log('as');
    next();
});
router.all('/logout', function(req, res, next){
    req.session.destroy();
    res.redirect('/users/login');
});

/* GET home page. */
router.get('/', function(req, res, next) {
    var sess=req.session;
    console.log(sess.username);
    var hostname="";
    if(!sess.username )
    {
        res.redirect('/users/login');
    }
    else{

        fs.readFile('J_DAT.json', 'utf8', function (err, data) {
            if (err) {
                return console.log(err);
            }

            try
            {
                var jObj=JSON.parse(data);
                hostname=jObj.host_name;
                console.log(hostname);

                sess.hostname=hostname;

            }
            catch( ex)
            {
                hostname="";
                sess.hostname=hostname;
            }


            res.render('index', { username: sess.username, title: 'API Test', hostname:hostname  });

        });
    }
});

router.get('/help', function(req, res, next) {
    var sess=req.session;
    if(!sess.username)
    {
        res.redirect('/users/login');
    }
    res.render('help', { title: 'Help', username: sess.username });

});

router.post('/updateCurrentHost', function(req, res, next) {
    var sess=req.session;
    if(!sess.username)
    {
        // res.redirect('/users/login');
    }

    var json_obj;
    fs.readFile('J_DAT.json', 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        json_obj = JSON.parse(data);
        json_obj.host_name=req.body.hostname;
        var dat = JSON.stringify(json_obj) ;
        fs.writeFile('J_DAT.json',dat , function (err) {
            if (err) {
                next(err);
                return console.log(err);
            }
        });
        res.send('success');
    });




});

router.get('/testGet', function(req, res, next) {

        console.log("get");
        res.send(req.query);

    }
);

router.post('/testPost', function(req, res, next) {

        console.log("post");
        res.send(req.body);
    }
);

router.delete('/testDelete', function(req, res, next) {

        console.log("delete");
        res.send(req.body);
    }
);

router.put('/testPut', function(req, res, next) {

        console.log("put");
        res.send(JSON.parse({'type':'put'}));
    }
);

module.exports = router;
