/**
 * Created by Asad on 7/07/15.
 */
//var acl = require('acl');
//
//
//acl = new acl(new acl.memoryBackend());
//acl.allow('admin', 'createEndpoint', 'view');
//acl.allow('admin', 'editEndpoint', 'view');
//acl.allow('admin', 'deleteEndpoint', 'view');
//
//module.exports = acl;

var acl = require('acl');
var acl = new acl(new acl.memoryBackend());

acl.allow([
    // Admin user
    {
        roles:'admin',
        allows:[
            {resources: ['/endpoints/viewEndpoints'], permissions:['get']},
            {resources: ['/endpoints/createEndpoint','/endpoints/updateEndpoint','/endpoints/deleteEndpoint','/endpoints/editEndpoint','/endpoints/viewEndpoints','/endpoints/getHostName','/endpoints/getEnpointsJSONData'], permissions:['get']},
            {resources: ['/endpoints/addNewEndpoint','/endpoints/updateEndpoint'], permissions:['post']}
        ]
    },    // Normal User
    {
        roles:'user',
        allows:[
            {resources: ['/endpoints/viewEndpoints','/endpoints/getHostName','/endpoints/getEnpointsJSONData'], permissions:['get']}
        ]
    }
]);

module.exports = acl;