    /**
     * Created by asad on 7/6/15.
     */
    hljs.initHighlightingOnLoad();
    $(".endpointsTab").addClass('active');

    var path = window.location.pathname;
    var page = path.split("/").pop();
    if(page=="viewEndpoints")
    {
        $( "#eps" ).removeClass( "collapse" );
    }

    function displayEndpoint(id) {
        $(".accordion").find(".epStyle").css('display', 'none');
    $("#"+id).css('display', 'inline');

    }

        function runAPITest(id,x) {
        //alert(id);
        var url=$(".accordion").find(".endpoint-name"+id).text();
        //url += "?";
        var datString="";
        var query_keys = $(".query-params"+id+" .form-inline");
        for (var jk = 0; jk < query_keys.length; jk++) {
            var key = $(query_keys[jk]).find(".key").val();
            var value = $(query_keys[jk]).find(".value").val();
            datString += key+"="+value;
            if(jk!=query_keys.length-1)
            {
                datString += "&";
            }
            //alert(key);
            //alert(value);
        }
        //alert(datString);
        var header_keys = $(".header-params" + id + " .form-inline");
        var header_string="";
        for (var jk = 0; jk < header_keys.length; jk++) {
            var key = $(header_keys[jk]).find(".key").val();
            var value = $(header_keys[jk]).find(".value").val();

            header_string += "'"+key + "':'" + value+"'";
            if (jk != header_keys.length - 1) {
                header_string += ",";
            }

            //alert(value);
        }
        //alert(header_string);


        //alert(url);
        var eType = $(".accordion").find("#ep"+id+"").text();
        //alert(eType);

         var ajaxObj ={};
         ajaxObj.headers={};
         ajaxObj.success=  function (data) {
             //$("#response").html(data);
             console.log('test');
             $(".accordion").find("#" + id).find("#apiResponseHead"+ id).remove();
             $(".accordion").find("#" + id).find("#apiResponseBody" ).remove();
             $(".accordion").find("#" + id).append("<div id='apiResponseHead"+ id+"' ><hr /><h4 class='greenBoldText'>Response</h4> </div><pre id='apiResponseBody'><hr /><code class='javascript'>"+library.json.prettyPrint((data))+"</code></pre>");
             $('html, body').animate({
                 scrollTop: $("#apiResponseHead"+ id).offset().top
             }, 500);
         };

            ajaxObj.url = url;

        if(eType=="POST")
        {
            ajaxObj.data= datString;
            ajaxObj.type="POST"; 
        }
        else if (eType=="GET")
        {
            ajaxObj.url=url+"?"+datString;
            ajaxObj.type="GET";
        }
        else if(eType=="PUT")
        {
            ajaxObj.data = datString;
            ajaxObj.type="PUT";
        }
        else if(eType=="DELETE") {
            ajaxObj.type = "DELETE";
        }
            console.log(ajaxObj);
            //alert(JSON.stringify(ajaxObj,null ,4));
        $.ajax(ajaxObj);
    }
    function toJSONQueryParams(id) {

        var query_parameters={};
        var query_keys = $(".query-params"+id+" .form-inline");
        for (var jk = 0; jk < query_keys.length; jk++) {
            var key = $(query_keys[jk]).find(".key").val();
            var value = $(query_keys[jk]).find(".value").val();
            try {
                query_parameters[key] = JSON.parse(value);
            }
            catch (er) {
                query_parameters[key] = value;
            }
        }
        document.getElementById('queryParamsTA'+id).value = JSON.stringify(query_parameters,null,4).replace(/\\/g,'');
    }
    function toJSONFormParams(id) {
        var form_parameters = {};
        var form_keys = $(".form-params" + id + " .form-inline");
        for (var jk = 0; jk < form_keys.length; jk++) {
            var key = $(form_keys[jk]).find(".key").val();
            var value = $(form_keys[jk]).find(".value").val();
            try {
                form_parameters[key] = JSON.parse(value);
            }
            catch (er) {
                form_parameters[key] = value;
            }
        }
        document.getElementById('formParamsTA' + id).value = JSON.stringify(form_parameters, null, 4);
    }

    function fromJSONQueryParams(id) {
        var jsonTA=$("#queryParamsTA"+id).val();
        var json_obj=JSON.parse(jsonTA);
        var subHtml = "";
        $.each(json_obj, function (k, v) {
            //display the key and value pair
            subHtml += "<div class='form-inline '>";
            subHtml += "<input type='text' class='key form - control keyValuePairs col-md-4'  value='" + k + "'/>";
            subHtml += "<input type='text' class='value form - control keyValuePairs col-md-7'  value='" + JSON.stringify(v) + "'/>";
            subHtml += "</div>";
        });
        $('.query-params'+id).html(subHtml);
    }



    function toJSONHeaderParams(id) {
        var header_parameters = {};
        var header_keys = $(".header-params" + id + " .form-inline");
        for (var jk = 0; jk < header_keys.length; jk++) {
            var key = $(header_keys[jk]).find(".key").val();
            var value = $(header_keys[jk]).find(".value").val();
            try
            {
                header_parameters[key] = JSON.parse(value);
            }
            catch(er)
            {
                header_parameters[key] =  value ;
            }
        }
        document.getElementById('headerParamsTA' + id).value = JSON.stringify(header_parameters, null, 4);
    }
    function fromJSONHeaderParams(id) {
        var jsonTA = $("#headerParamsTA" + id).val();
        var json_obj = JSON.parse(jsonTA);
        var subHtml = "";
        $.each(json_obj, function (k, v) {
            //display the key and value pair
            subHtml += "<div class='form-inline '>";
            subHtml += "<input type='text' class='key form - control keyValuePairs col-md-4'  value='" + k + "'/>";
            subHtml += "<input type='text' class='value form - control keyValuePairs col-md-7'  value='" + JSON.stringify(v) + "'/>";
            subHtml += "</div>";
        });
        $('.header-params' + id).html(subHtml);
    }





    function fromJSONFormParams(id) {
        var jsonTA = $("#formParamsTA" + id).val();
        var json_obj = JSON.parse(jsonTA);
        var subHtml = "";
        $.each(json_obj, function (k, v) {
            //display the key and value pair
            subHtml += "<div class='form-inline '>";
            subHtml += "<input type='text' class='key form - control keyValuePairs col-md-4'  value='" + k + "'/>";
            subHtml += "<input type='text' class='value form - control keyValuePairs col-md-7'  value='" + JSON.stringify(v)+ "'/>";
            subHtml += "</div>";
        });
        $('.form-params' + id).html(subHtml);
    }


    $.getJSON("getEnpointsJSONData", function (data) {
        //data=JSON.stringify(data);
        var host = location.search.split('host=')[1];
        $.getJSON("getHostName", function (host)
        {
            var renderingDat = "";
            for (var i = 0; i < data.length; i++) {
                var obj = data[i];
                var subHtml="";
                subHtml += "<div class='epStyle' style='display:none' id='" + i + "'>";
                //// Uses the hidden hostname for the api test
                subHtml += "<h4 style=''><b><span id='ep"+i+"'>"+obj.endpoint_type+"</span></b>:&nbsp&nbsp<span   class='endpoint-name" + i + "  textGreen'>" + host.host_name + obj.endpoint_name + "</span>";
                if("#{session.role}" != "user")
                {
                    subHtml +=
                        "<a href='deleteEndpoint?uid=" + obj.uid + "' onclick='return confirm(\"Are you sure ?\");' style='margin-right:10px;' class='pull-right flat-bt-small flat-danger-bt' style='font-size:12px;'>Delete</a>" ;
                    subHtml +=       "<a href='editEndpoint?uid=" + obj.uid + "' style='margin-right:10px;' class='pull-right flat-bt-small flat-warning-bt' style='font-size:12px;'>Edit</a>" ;
                }
                subHtml +=        "<a onclick='runAPITest(" + i + ",this)' style='margin-right:10px;'  class='pull-right flat-bt-small flat-success-bt'>Run</a></h4>";


                subHtml += "<br />" + "<h5>Headers:</h5><hr class='greyHr'/>";

                subHtml += "<div class='row'><div class='header-params" + i + " col-md-6 ' style='padding-left: 30px;'>";
                $.each(obj.header_parameters, function (k, v) {
                    //display the key and value pair
                    subHtml += "<div class='form-inline '>";
                    subHtml += "<input type='text' class='key form - control keyValuePairs col-md-4 '  value='" + k + "'/>";
                    subHtml += "<input type='text' class='value form - control keyValuePairs col-md-7'   value='" + v + "'/>";
                    subHtml += "</div>";
                });
                subHtml += "</div>";
                subHtml += "<div class=' col-md-1'  >";
                subHtml += "<a onclick='toJSONHeaderParams(" + i + ")' style='text-align:left; width:90px' class=' '> <span class='fa fa-3x fa-arrow-circle-o-right pointerCursor blueFont'></span></a></h4><br /><br />";
                subHtml += "<a onclick='fromJSONHeaderParams(" + i + ")'  style='text-align:center; width:90px' class='  '><span class='fa fa-3x fa-arrow-circle-o-left pointerCursor blueFont'></span></a></h4>";
                subHtml += "</div>";
                subHtml += "<div class=' col-md-4'   >";
                subHtml += "<textarea id='headerParamsTA" + i + "' rows='5' cols='40'/>";
                subHtml += "</div>";
                subHtml += "</div> ";


                subHtml += "<br />" + "<h5>Query Parameters:</h5><hr class='greyHr'/>";
                subHtml += "<div class='row'><div class='query-params" + i + " col-md-6 ' style='padding-left: 30px;'>";
                $.each(obj.query_parameters, function (k, v) {
                    //display the key and value pair
                    subHtml += "<div class='form-inline '>";
                    subHtml += "<input type='text' class='key form - control keyValuePairs col-md-4 '  value='" + k + "'/>";
                    subHtml += "<input type='text' class='value form - control keyValuePairs col-md-7'   value='" + v + "'/>";
                    subHtml += "</div>";
                });
                subHtml += "</div>";
                subHtml += "<div class=' col-md-1'  >";
                subHtml += "<a onclick='toJSONQueryParams(" + i + ")' style='text-align:left; width:90px' class=' '> <span class='fa fa-3x fa-arrow-circle-o-right pointerCursor blueFont'></span></a></h4><br /><br />";
                subHtml += "<a onclick='fromJSONQueryParams(" + i + ")'  style='text-align:center; width:90px' class='  '><span class='fa fa-3x fa-arrow-circle-o-left pointerCursor blueFont'></span></a></h4>";

                subHtml += "</div>";
                subHtml += "<div class=' col-md-4'   >";
                subHtml += "<textarea id='queryParamsTA" + i + "' rows='5' cols='40'/>";
                subHtml += "</div>";
                subHtml += "</div><h5>Form Parameters:<hr class='greyHr'/></h5>";




                subHtml += "<div class='row'><div class='form-params" + i + " col-md-6 ' style='padding-left: 30px;'>";
                $.each(obj.form_parameters, function (k, v) {
                    //display the key and value pair
                    subHtml += "<div class='form-inline '>";
                    subHtml += "<input type='text' class='key form - control keyValuePairs col-md-4 '  value='" + k + "'/>";
                    subHtml += "<input type='text' class='value form - control keyValuePairs col-md-7'   value='" + v + "'/>";
                    subHtml += "</div>";
                });
                subHtml += "</div>";
                subHtml += "<div class=' col-md-1'  >";
                subHtml += "<a onclick='toJSONFormParams(" + i + ")' style='text-align:left; width:90px' class=' '> <span class='fa fa-3x fa-arrow-circle-o-right pointerCursor blueFont'></span></a></h4><br /><br />";
                subHtml += "<a onclick='fromJSONFormParams(" + i + ")'  style='text-align:center; width:90px' class='  '><span class='fa fa-3x fa-arrow-circle-o-left pointerCursor blueFont'></span></a></h4>";
                subHtml += "</div>";
                subHtml += "<div class=' col-md-4'   >";
                subHtml += "<textarea id='formParamsTA" + i + "' rows='5' cols='40'/>";
                subHtml += "</div>";
                subHtml += "</div> ";




                subHtml += "<h5>Description:<hr class='greyHr'/></h5>";
                subHtml += "<div style='padding-left: 50px;'>";
                subHtml += obj.endpoint_description;
                subHtml += "</div><br/>";

                subHtml += "<a onclick='runAPITest(" + i + ",this)' class='pull-right flat-bt flat-success-bt'>Run</a><br/><br/></div>";
                $('.accordion').append(subHtml);

            }
            $(function () {
                $(".accordion").accordion({active: false, collapsible: true});
            });

        });

    });
