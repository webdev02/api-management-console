var express = require('express');
var router = express.Router();
var uuid = require('uuid');
var acl = require('../config/acl');
var crypto = require('crypto');

var consts = require('../config/constants.js');
/* GET users listing. */

router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Login' });
});

router.post('/processLoginRequest', function(req, res, next) {
    var user = req.param("username");
    var pass = req.param("password");
  console.log("user="+user);
  console.log("pass="+pass);
  /*
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.write(user + " " +pass);
  res.end();
 */
    var passwordHash=crypto.createHash('md5').update(pass).digest("hex");
    if(user=="admin" && passwordHash==consts.adminPassword )
    {
        var sess=req.session;
        sess.username=user;
        sess.role="admin";
        sess.userId = uuid.v4();
        acl.addUserRoles(sess.userId,sess.role,function(err,roles){
            if(err) console.log(err);
            else {
                console.log(JSON.stringify(roles));
                res.send('success');
            }
        });
    }
    else if(user=="user" && passwordHash==consts.userPassword)
    {
        var sess=req.session;
        sess.username=user;
        sess.role="user";
        sess.userId = uuid.v4();
        acl.addUserRoles(sess.userId,sess.role,function(err,roles){
            if(err) console.log(err);
            else {
                console.log(JSON.stringify(roles));
                res.send('success');
            }
        });
    }
    else
    {
        res.send('fail');
    }
});
module.exports = router;
