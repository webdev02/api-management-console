/**
 * Created by asad on 6/25/15.
 */
$(function(){

    $('#editEndpoint').click(function(e) {

        e.preventDefault();


        var uid=$("body").find(".uid").val();
        var query_keys = $(".query-parameters .parameter-row");
        var json_arr=[];
        var json_obj = {};
        json_obj.endpoint_name=$(".form-group").find(".endpointName").val();
        json_obj.endpoint_type=$(".form-group").find("input[type='radio'][name='endpointType']:checked").val();;

        json_obj.uid = uid;
        json_obj.query_parameters ={};
        for(i = 0; i < query_keys.length; i++) {
            var key = $(query_keys[i]).find("#inputKey").val();
            var value = $(query_keys[i]).find("#inputValue").val();
            console.log(key);
            console.log(value);

            if (key != "") {
                json_obj["query_parameters"][key] = value;
            }
        }


        var form_keys = $(".form-parameters .parameter-row");
        json_obj.form_parameters = {};
        for(i = 0; i < form_keys.length; i++){
            var key = $(form_keys[i]).find("#inputKey").val();
            var value = $(form_keys[i]).find("#inputValue").val();
            console.log(key);
            console.log(value);
            if (key != "") {
                json_obj.form_parameters[key] = value;
            }
        }


        var header_keys = $(".header-parameters .parameter-row");
        json_obj.header_parameters = {};
        for(i = 0; i < header_keys.length; i++){
            var key = $(header_keys[i]).find("#inputKey").val();
            var value = $(header_keys[i]).find("#inputValue").val();
            console.log(key);
            console.log(value);
            if (key != "") {
            json_obj.header_parameters[key]=value;}
        }


        json_obj.endpoint_description=$(".form-group").find(".endpointDescription").val();
        json_arr.push(json_obj);

        console.log(json_obj);
        var strJson=JSON.stringify(json_obj,null,4);
        console.log(strJson);

        var dataString = json_obj;
        $.ajax({
            type: 'POST',
            url: 'updateEndpoint',
            data: {json_obj : strJson},
            success: function(data) {
                if(data=='success')
                {
                    // $("#err").removeClass( "errorMessage").addClass('successMessage');
                    // $("#err").html("Endpoint Successfully Created <br /><a href='createEndpoint' >Add Another</a>&nbsp;&nbsp;&nbsp;or&nbsp;&nbsp;&nbsp;<a href='/' >Go Back</a>");

                    window.location.replace("viewEndpoints");
                }
                else
                {
                    $("#err").removeClass( "successMessage").addClass('errorMessage');
                    $("#err").html("Error!<br />  <a href='createEndpoint' >Add Another</a>&nbsp;&nbsp;&nbsp;or&nbsp;&nbsp;&nbsp;<a href='/' >Go Back</a>");
                }
            }
        });





    });

    $('#createEndpoint').click(function(e) {
        e.preventDefault();

        var query_keys = $(".query-parameters .parameter-row");
        var json_arr=[];
        var json_obj = {};
        json_obj.endpoint_name=$(".form-group").find(".endpointName").val();
        json_obj.endpoint_type=$(".form-group").find("input[type='radio'][name='endpointType']:checked").val();;


        json_obj.query_parameters ={};
        for(i = 0; i < query_keys.length; i++){
            var key = $(query_keys[i]).find("#inputKey").val();
            var value = $(query_keys[i]).find("#inputValue").val();
            console.log(key);
            console.log(value);


            json_obj["query_parameters"][key]=value;
        }


        var form_keys = $(".form-parameters .parameter-row");
        json_obj.form_parameters = {};
        for(i = 0; i < form_keys.length; i++){
            var key = $(form_keys[i]).find("#inputKey").val();
            var value = $(form_keys[i]).find("#inputValue").val();
            console.log(key);
            console.log(value);
            json_obj.form_parameters[key]=value;
        }

        var header_keys = $(".header-parameters .parameter-row");
        json_obj.header_parameters = {};
        for(i = 0; i < header_keys.length; i++){
            var key = $(header_keys[i]).find("#inputKey").val();
            var value = $(header_keys[i]).find("#inputValue").val();
            console.log(key);
            console.log(value);
            json_obj.header_parameters[key]=value;
        }


        json_obj.endpoint_description=$(".form-group").find(".endpointDescription").val();
        json_arr.push(json_obj);

        console.log(json_obj);
        var strJson=JSON.stringify(json_obj,null,4);
        console.log(strJson);


        var dataString = json_obj;
        $.ajax({
            type: 'POST',
            url: 'addNewEndpoint',
            data: {json_obj : strJson},
            success: function(data) {
                if(data=='success')
                {
                   // $("#err").removeClass( "errorMessage").addClass('successMessage');
                   // $("#err").html("Endpoint Successfully Created <br /><a href='createEndpoint' >Add Another</a>&nbsp;&nbsp;&nbsp;or&nbsp;&nbsp;&nbsp;<a href='/' >Go Back</a>");

                    window.location.replace("viewEndpoints");
                }
                else
                {
                    $("#err").removeClass( "successMessage").addClass('errorMessage');
                    $("#err").html("Error!<br />  <a href='createEndpoint' >Add Another</a>&nbsp;&nbsp;&nbsp;or&nbsp;&nbsp;&nbsp;<a href='/' >Go Back</a>");
                }
            }
        });




    });

    $('#login').click(function(e){
        e.preventDefault();
        var dataString = 'username='+$('#username').val()+'&password='+$('#password').val();
        $.ajax({
            type: 'POST',
            url: 'processLoginRequest',
            data: dataString,
            success: function(data) {
                if(data=='success')
                {
                    $("#err").removeClass( "errorMessage").addClass('successMessage');
                    $("#err").html("Authenticated");
                    window.location.replace("/");
                }
                else
                {
                    $("#err").removeClass( "successMessage").addClass('errorMessage');
                    $("#err").html("Authentication Failed!");
                }
            }
        });

    });



    $('#updateHost').click(function(e){
        e.preventDefault();
        var host=$('#hostname').val();
        if (host.substring(0, 7) == "http://" || host.substring(0, 8) == "https://") {
        }
        else{
            host =  "http://"+host;
        }
        var dataString = 'hostname='+host ;
        $.ajax({
            type: 'POST',
            url: 'updateCurrentHost',
            data: dataString,
            success: function(data) {
                if(data=='success')
                {
                    $("#err").removeClass( "errorMessage").addClass('successMessage');
                    window.location.replace("/");
                    $("#err").html("Host Updated");
                }
                else
                {
                    $("#err").removeClass( "successMessage").addClass('errorMessage');
                    $("#err").html("Error!");
                }
            }
        });

    });



});


var counterHeaderParamPairs = 0;

function addHeaderParamPair(divName) {

    counterHeaderParamPairs++;
    var newdiv = document.createElement('div');
    newdiv.className = "form-group row parameter-row";
    newdiv.setAttribute("id", "headerParamsPair" + (counterHeaderParamPairs) + "");newdiv.innerHTML =
        " "
        + " "
        + "<div class='col-md-4'>"
        + "     <input type='text' class='form-control' id='inputKey' name='headerParamsKey[]'  placeholder='Key'>"
        + "</div>"
        + "<div class='col-md-4'>"
        + "     <input type='text' class='form-control' id='inputValue'  name='headerParamsValue[]' placeholder='Value'>"
        + "</div>"
        + "<div class='col-md-1'>"
        + "     <input type='button' value='+'  class='flat-bt flat-info-bt addRemoveBt' onClick='addHeaderParamPair(&quot;headerParams&quot;);'/>"
        + "</div>"
        + "<div class='col-md-1'>"
        + "     <input type='button' value='-'  class='flat-bt flat-danger-bt addRemoveBt' onClick='removeHeaderParamPair(&quot;headerParamsPair" + (counterHeaderParamPairs) + "&quot;,this);'/>"
        + "</div>" ;
    document.getElementById(divName).appendChild(newdiv);


}
function removeHeaderParamPair(divName, x) {
    $(x).parent().parent().remove();
    //var element = document.getElementById(divName);
    //element.parentNode.removeChild(element);

}



var counterGETParamPairs = 0;

function addGetParamPair(divName) {

    counterGETParamPairs++;
    var newdiv = document.createElement('div');
    newdiv.className = "form-group row parameter-row";
    newdiv.setAttribute("id", "getParamsPair" + (counterGETParamPairs) + "");newdiv.innerHTML =

          "<div class='col-md-4'>"
        + "     <input type='text' class='form-control' id='inputKey' name='getParamsKey[]'  placeholder='Key'>"
        + "</div>"
        + "<div class='col-md-4'>"
        + "     <input type='text' class='form-control' id='inputValue'  name='getParamsValue[]' placeholder='Value'>"
        + "</div>"
        + "<div class='col-md-1'>"
        + "     <input type='button' value='+'  class='flat-bt flat-info-bt addRemoveBt' onClick='addGetParamPair(&quot;getParams&quot;);'/>"
        + "</div>"
        + "<div class='col-md-1'>"
        + "     <input type='button' value='-'  class='flat-bt flat-danger-bt addRemoveBt' onClick='removeGetParamPair(&quot;getParamsPair" + (counterGETParamPairs) + "&quot;,this);'/>"
        + "</div>" ;
    document.getElementById(divName).appendChild(newdiv);


}
function removeGetParamPair(divName,x) {

    $(x).parent().parent().remove();
   // var element = document.getElementById(divName);
    //element.parentNode.removeChild(element);

}



var counterPostParamPairs = 0;

function addPostParamPair(divName) {

    counterPostParamPairs++;
    var newdiv = document.createElement('div');
    newdiv.className = "form-group row parameter-row";
    newdiv.setAttribute("id", "postParamsPair" + (counterPostParamPairs) + "");
    newdiv.innerHTML =
        "<div class='col-md-4'>"
        + "     <input type='text' class='form-control' id='inputKey' name='postParamsKey[]'  placeholder='Key'>"
        + "</div>"
        + "<div class='col-md-4'>"
        + "     <input type='text' class='form-control' id='inputValue'  name='postParamsValue[]' placeholder='Value'>"
        + "</div>"
        + "<div class='col-md-1'>"
        + "     <input type='button' class='flat-bt flat-info-bt addRemoveBt' value='+' onClick='addPostParamPair(&quot;postParams&quot;);'/>"
        + "</div>"
        + "<div class='col-md-1'>"
        + "     <input type='button'  class='flat-bt flat-danger-bt addRemoveBt' value='-' onClick='removePostParamPair(&quot;postParamsPair" + (counterPostParamPairs) + "&quot;,this);'/>"
        + "</div>" ;
    document.getElementById(divName).appendChild(newdiv);


}
function removePostParamPair(divName,x) {

    $(x).parent().parent().remove();
    //var element = document.getElementById(divName);
    //element.parentNode.removeChild(element);

}
