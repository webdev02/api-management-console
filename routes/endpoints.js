/**
 * Created by asad on 6/25/15.
 */
var express = require('express');
var fs = require('fs');
var uuid = require('uuid');
var router = express.Router();
var acl = require('../config/acl');

/* GET users listing. */

router.get('/createEndpoint', acl.middleware(), function(req, res, next) {
    var sess=req.session;
    if(!sess.username)
    {
       res.redirect('/users/login');
    }
    else
    {
        if(sess.role == "admin")
        {
            res.render('createEndpoint', { title: 'Create Endpoint', username: sess.username });
        }
        else
        {
            res.render('viewEndpoints', { title: 'View Endpoints', username: sess.username });
        }
    }



});


router.get('/deleteEndpoint', acl.middleware(), function(req, res, next) {
    var sess=req.session;
    if(!sess.username)
    {
        res.redirect('/users/login');
    }
    if(sess.role !="admin")
    {
        res.redirect('viewEndpoints');
    }
    else {
        var uid = req.query.uid;
        var reqObj = {};
        fs.readFile('J_DAT.json', 'utf8', function (err, data) {
            if (err) {
                return console.log(err);
            }
            console.log("READ READ" + data);
            var json_obj = JSON.parse(data);

            console.log("DAT DAT" + json_obj.endpoints.length);

            for (i = 0; i < json_obj.endpoints.length; i++) {
                console.log("WD");
                console.log(json_obj.endpoints[i].uid);
                if (json_obj.endpoints[i].uid == uid) {
                    reqObj = json_obj.endpoints[i];
                    console.log(reqObj);
                    json_obj.endpoints.splice(i, 1);
                    fs.writeFile('J_DAT.json', JSON.stringify(json_obj), function (err) {
                        if (err) {
                            next(err);
                            return console.log(err);
                        }
                    });
                }
            }

        });
    }
    res.redirect('viewEndpoints');


});




router.get('/editEndpoint', acl.middleware(), function(req, res, next) {
    var sess=req.session;
    if(!sess.username)
    {
        res.redirect('/users/login');
    }
    if(sess.role !="admin")
    {
        res.redirect('viewEndpoints');
    }
    var uid=req.query.uid;
    var reqObj={};
    fs.readFile('J_DAT.json', 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        console.log("READ READ"+data);
        var json_obj=JSON.parse(data);

        console.log("DAT DAT"+ json_obj.length);

        for(i=0;i<json_obj.endpoints.length;i++)
        {

            if(json_obj.endpoints[i].uid==uid)
            {
                reqObj=json_obj.endpoints[i];
                console.log(reqObj);

                res.render('editEndpoint', { title: 'Edit Endpoint',obj:reqObj, username: sess.username });

            }
        }

    });


});

router.post('/updateEndpoint', acl.middleware(), function(req, res, next) {
    var sess = req.session;
    if (!sess.username) {
        res.redirect('/users/login');
    }
    try {
        var dat = req.body.json_obj;

        fs.readFile('J_DAT.json', 'utf8', function (err, data) {
   {

       var newEndpoint = JSON.parse(dat);
       fs.readFile('J_DAT.json', 'utf8', function (err, data) {
                    if (err) {
                        return console.log(err);
                    }
                    console.log("READ READ" + data);
                    console.log("DAT DAT" + dat);
                    var json_obj = JSON.parse(data);

                    for(i=0;i<json_obj.endpoints.length;i++)
                    {

                        if(json_obj.endpoints[i].uid==newEndpoint.uid)
                        {
                            reqObj=json_obj.endpoints[i];
                            console.log(reqObj);
                            json_obj.endpoints.splice(i,1);
                        }
                    }
           newEndpoint.uid=newEndpoint.uid;
                    json_obj.endpoints.push(newEndpoint);
                    console.log(json_obj);
                    fs.writeFile('J_DAT.json', JSON.stringify(json_obj), function (err) {
                        if (err) {
                            next(err);
                            return console.log(err);
                        }
                        console.log(dat + ' > J_DAT.txt');
                    });

                    res.send('success');
                });


            }
        });
    }
    catch (err) {
        next(err);
    }
});

router.get('/viewEndpoints', acl.middleware(), function(req, res, next) {
    var sess=req.session;
    if(!sess.username)
    {
         res.redirect('/users/login');
    }

        res.render('viewEndpoints', { title: 'View Endpoints', username: sess.username });


});

router.get('/getHostName', acl.middleware(), function(req, res, next) {
    var sess=req.session;
    if(!sess.username)
    {
        res.redirect('/users/login');
    }
    try{
        fs.readFile('J_DAT.json', 'utf8', function (err,data) {
            if (err) {
                return console.log(err);
            }
            console.log("READ READ"+data);
            var json_obj=JSON.parse(data);

            var host={};
            host.host_name=json_obj.host_name;
             res.send(JSON.stringify(host));
        });
    }
    catch(err)
    {
        res.send("");
    }

});


router.get('/getEnpointsJSONData', acl.middleware(), function(req, res, next) {
    var sess=req.session;
    if(!sess.username)
    {
        res.redirect('/users/login');
    }

    fs.readFile('J_DAT.json', 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        console.log("READ READ"+data);
        var json_obj=JSON.parse(data);

        console.log("DAT DAT"+ json_obj.length);

        for(i=0;i<json_obj.length;i++)
        {

        }

        res.send(JSON.stringify(json_obj.endpoints));
    });

});

router.post('/addNewEndpoint', acl.middleware(), function(req, res, next) {
    var sess=req.session;
    if(!sess.username)
    {
        res.redirect('/users/login');
    }
    try {
        var dat =   req.body.json_obj;

        fs.readFile('J_DAT.json', 'utf8', function (err,data) {
            if (err) {
                var json_obj={};

                json_obj.endpoints=[];

                var newEndpoint=JSON.parse(dat);
                newEndpoint.uid=uuid.v1();


                json_obj.endpoints.push(newEndpoint);

                fs.writeFile('J_DAT.json', JSON.stringify(json_obj), function (err) {
                    if (err) {
                        next(err);
                        return console.log(err);
                    }
                    console.log(dat + ' > J_DAT.txt');
                });
                res.send('success');
            }
            else
            {
                fs.readFile('J_DAT.json', 'utf8', function (err,data) {
                    if (err) {
                        return console.log(err);
                    }
                    console.log("READ READ"+data);
                    console.log("DAT DAT"+dat);
                    var json_obj=JSON.parse(data);

                    var newEndpoint=JSON.parse(dat);
                    newEndpoint.uid=uuid.v1();

                    json_obj.endpoints.push(newEndpoint);


                    console.log(json_obj);
                    fs.writeFile('J_DAT.json',JSON.stringify(json_obj), function (err) {
                        if (err) {
                            next(err);
                            return console.log(err);
                        }
                        console.log(dat + ' > J_DAT.txt');
                    });

                    res.send('success');
                });



            }
        });
    }
    catch(err){
        next(err);
    }
});
module.exports = router;
